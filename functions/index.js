const functions = require("firebase-functions");
const admin = require('firebase-admin');

const app = admin.initializeApp();

exports.validator = functions.https.onRequest((request, response) => {
  // setting cors allow all
  response.set('Access-Control-Allow-Origin', '*');
  response.set('Access-Control-Allow-Methods', 'GET, POST');
  response.set('Access-Control-Allow-Headers', 'Content-Type');

  if(request.method === "POST"){
    let data = request.body;
    if(typeof request.body === "string") data = JSON.parse(request.body);

    const idToken =(data.token).toString();
    // validation check goes here
    admin.auth().verifyIdToken(idToken)
    .then((decodedToken) => {
      response.status(200).send({
        verified: true,
        decodedToken
      })
    })
    .catch((error) => {
      response.status(401).send({
        verified: false,
        error
      })
  })
} else {
  response.json({
   About:"Takes token and check if it is valid or not",
   Info:"POST data with key value as token: <Value of token>"
 })
}
});
