import React, {useState} from "react";
import { initializeApp } from "firebase/app";
import { getAuth, signInWithPopup, GoogleAuthProvider, getIdToken, signOut  } from "firebase/auth";
import {firebaseConfig} from "./Firebase/config"
import axios from "axios"
import firebase from "firebase/auth";

import GoogleButton from 'react-google-button'
import 'bootstrap/dist/css/bootstrap.min.css';

import Alert from 'react-bootstrap/Alert'

function App() {
  const [tokenD, setTokenD]= useState("Sign in with google to see token");
  const [userD, setuserD]= useState("Sign in or enter token to see user email");
  const [hide, sethide]= useState(true);
  const [message, setmessage] = useState();
  const [variant, setvarient] = useState(false);

  initializeApp(firebaseConfig);
  const provider = new GoogleAuthProvider();
  const auth = getAuth();

  const signin = ()=>{
    signInWithPopup(auth, provider)
      .then((result) => {
        const user = result.user;
        user.getIdToken().then(function(idToken) {
          setTokenD(idToken);
          validateToken(idToken);
        }).catch(function(error) {
          // Handle error
          console.log(error);
        });

        sethide(!hide);

      }).catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.email;
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        // ...
      });
  }

  const validateToken=async (data)=>{

    try{
      const res = await axios.post("https://us-central1-thriftywallet-ee938.cloudfunctions.net/validator",{
        "token":data
      })
      if(res.data.verified){
        sethide(false);
        setvarient(true);
        setmessage("Token Valid");
        setuserD(res.data.decodedToken.email);
      }
    } catch(error){
    if(!error.response.data.verified){
      setvarient(false);
      sethide(false);
      setmessage("Token Invalid");
    }
    }
  }

  return (
    <div>
      <div className="m-5">
        <h2>Login or validate</h2>
        <div className="d-flex justify-content-center m-4">
          <GoogleButton
            onClick={signin}
          />

          <div className="mx-4" style={hide ? {display: "none"} : {display:"initial"}}>
            <button className="btn btn-primary btn-lg" onClick={()=>{
              signOut(auth).then(() => {
                // Sign-out successful.
                setTokenD("Sign in with google to see token")
                setuserD("Sign in or enter token to see user email")
                sethide(!hide);
              }).catch((error) => {
                // An error happened.
                console.log(error, "signOut");
              });
            }}> Reset </button>
          </div>
        </div>
        <h2 className="text-center">or</h2>
        <div className="d-flex justify-content-center">
          <div className="input-group mb-3" style={{width:"60%"}}>
            <input type="text" className="form-control" placeholder="Token" name="token"/>
            <div className="input-group-append ">
              <button className="btn btn-outline-primary" type="button" onClick={()=>{
                validateToken(document.querySelector("input[name='token']").value);
                document.querySelector("input[name='token']").value="";
              }}>Check</button>
            </div>
          </div>
        </div>
      </div>

      <div style={hide ? {display: "none"} : {display:"initial"}}>
        <div className="p-5" >
          <Alert variant={variant ? "success" : "danger"}>
            {message}
          </Alert>
        </div>
      </div>

      <div className="m-2">
        <h2>Token</h2>
        <h5 className="p-4">{tokenD}</h5>

        <h2 className="mt-5">User Email</h2>
        <h5 className="p-4">{userD}</h5>
      </div>
    </div>
  );
}

export default App;
